<?
// ----------------------------------
// This is a sample model w/ a sample
// use of the included mysql class
// ----------------------------------

class notes
{
    public function __construct()
    {
    }

    public function getNotes($date, $userid)
    {
        $sql = "SELECT n.ID, n.Title, n.Body FROM calendar c JOIN note n ON n.ID = c.noteID WHERE c.AddUserID = " . $userid . " AND CalendarDay = " . "CAST(" . mysql::quote($date) . " AS DATE) ";
//        echo $sql;
        // SELECT n.ID, n.Title, n.Body FROM calendar c JOIN note n ON n.ID = c.noteID Where c.AddUserID = 8
//        echo $sql;
        $result = mysql::query('main', $sql);
//		if(PEAR::isError($result) || !$result)
//		{
//			return FALSE;
//		}
        return $result;
    }

    public function insertUpdateNote($noteid, $title, $body, $userid, $date)
    {
        //creates a new note if the ID is 0 (meaning it was just created by the AddNote button
        if ($noteid == 0) {

            //Inserts into note
            $sql = "INSERT INTO `note`(`Title`, `Body`, `AddUserID`, `AddDate`, `LastUpdateUserID`, `LastUpdate`) VALUES (" . mysql::quote($title) . ", " . mysql::quote($body) . ", " . $userid . ", NOW(), " . $userid . ", NOW())";

            mysql::query('main', $sql);
            $noteid = mysql::last_insert_id('main');

            // Inserts into calendar
            $sql = "INSERT INTO `calendar`(`CalendarDay`, `NoteID`, `AddUserID`, `AddDate`, `LastUpdateUserID`, `LastUpdate`) VALUES (" . mysql::quote($date) . ", " . $noteid . ", " . $userid . ", NOW(), " . $userid . ", NOW())";
            mysql::query('main', $sql);
        } else {

            // Updates an already existing note
            $sql = "UPDATE `note` SET `Title`=" . mysql::quote($title) . ",`Body`=" . mysql::quote($body) . ",`LastUpdateUserID`=" . $userid . ",`LastUpdate`= NOW() WHERE `ID` = " . $noteid;
            mysql::query('main', $sql);
        }
        //PEAR was having an issue with it being a static call, so I commented it out
//		if(PEAR::isError($result) || !$result)
//		{
//			return FALSE;
//		}
    }

    //removes a note
    public function removeNote($noteid)
    {
        $sql = "DELETE FROM `calendar` WHERE `NoteID` = " . $noteid;
        mysql::query('main', $sql);
        $sql = "DELETE FROM `note` WHERE `ID` = " . $noteid;
        mysql::query('main', $sql);
    }
}

?>
