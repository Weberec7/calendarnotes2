<?

class calendar_controller extends base_controller
{

    public function index()
    {
        $this->include_css_file("/css/calendar.css");
        $this->sessionUser = $_SESSION['userid'];
        $this->paramUser = $_GET["userid"];
    }

    public function mycalendar()
    {
        $this->noteArray = $this->getNote("this");
        $this->include_css_file("/css/calendar.css");
        $this->sessionUser = $_SESSION['userid'];
        $this->paramUser = $_GET["userid"];

        // compare url and parameter userid.  If they are not the same, redirect the user.
        if ($_GET["userid"] != $_SESSION['userid']) {
            header('Location: /calendar/index');
        }
    }

    public function _error()
    {

    }

    //This method is responsible for getting the Notes from the model (getNote) and setting the view
    public function showNotes()
    {
        $this->disable_layout();
        if ($_POST["date"]) {
            $date = $_POST["date"];
            //get Notes from the view
            $result = $this->getNote($date);
            $this->noteArray = $result;
            $_SESSION['noteArray'] = $result;
            $this->set_view("notes");
        }
    }

    //This function collects the POST data from the client and sends it to the model to insert/update a note
    public function updateNote()
    {
        $this->disable_layout();
        if ($_POST["body"] && $_POST["title"]) {
            $noteModel = new notes();
            $noteid = $_POST["id"];
            $body = $_POST["body"];
            $title = $_POST["title"];
            $userid = $_SESSION['userid'];
            $date = $_POST['date'];
            $noteModel->insertUpdateNote($noteid, $title, $body, $userid, $date);
        }

        // Updates Notes on the client after the insert/update completes
        $this->showNotes();
    }

    //Tells the model to remove a note, then updates the view.
    public function removeNote()
    {
        $this->disable_layout();
        $noteModel = new notes();
        $noteid = $_POST["id"];
        $date = $_POST['date'];
        $noteModel->removeNote($noteid);

        $this->showNotes();
    }

    //Tells the model to get all the notes for a given day/user
    public function getNote($date)
    {
        $noteModel = new notes();
        $result = $noteModel->getNotes($date, $_SESSION['userid']);
        //$note = new note(1,$date, "A long winded body.");
        $noteArray = array();
        if ($result) {

            foreach ($result as $row) {
                array_push($noteArray, new note($row['ID'], $row['Title'], $row['Body']));
            }
        }
        //array_push($noteArray, new note(2,"Test Title2", "A long winded body2."));
        return $noteArray;
    }


}


// a php note class only needed for calendar.php.  This provides note objects.
class note
{
    public $id;
    public $title;
    public $body;
    public $html;

    public function __construct($ID, $TITLE, $BODY)
    {
        $this->id = $ID;
        $this->title = $TITLE;
        $this->body = $BODY;

        //The html required to view a note
        $this->html =
            '<div id="Note' . $ID . '" class="Notes">
            <div id="contentNote' . $ID . '" contenteditable="false">
            <h3 id="noteTitle">' . $TITLE . '</h3>

            <p id="noteBody">' . $BODY . '</p>
            </div>
            <div>
            <a class="edit">Edit</a>
            <a class="remove">Remove</a>
            </div>
        </div>';
        return true;
    }
}

?>
