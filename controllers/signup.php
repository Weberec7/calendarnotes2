<?

class signup_controller extends base_controller
{
    public function index()
    {


    }

    public function post()
    {
        $signupModel = new signup();
        $this->disable_layout();
        $this->set_view("post");

        //gets the form information
        if ($_POST["email"] && $_POST["password"] && $_POST['cemail'] && $_POST['cpassword']) {
            $email = $_POST["email"];
            $password = $_POST["password"];
            $cemail = $_POST['cemail'];
            $cpassword = $_POST['cpassword'];

            //check to see if passwords and emails are the same
            if ($cpassword == $password && $email = $cemail) {
                //encrypt password before creating the user in the singupModel
                $signupModel->createUser($email, crypto::encrypt("salt", $password));
            }
        }

    }


}

?>
