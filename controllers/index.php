<?

class index_controller extends base_controller
{
    public function index()
    {
        // ------------------------------------------------------
        // This is where you'd put the logic for your index page
        // For this sample site, there isn't much so we're going
        // to ust set a variable and then print it out in the
        // view, which can be found in views/index/index.phtml
        // ------------------------------------------------------
        $this->foo = "This is the value off \$this->foo set in the index_controller and function index()";
    }

    public function login()
    {
        //used to suppress warnings from crypto
        error_reporting(E_ERROR | E_PARSE);
        $crypt = new crypto();
        $result = "";
        $loginModel = new login();
        $this->disable_layout();
        if ($_POST["email"] && $_POST["password"]) {
            $email = $_POST["email"];
            $password = $_POST["password"];
            $result = $loginModel->login($email, $crypt->encrypt("salt", $password));
            if ($result) {

                $_SESSION['userid'] = &$result;
                $_SESSION['username'] = $email;
                $this->set_view("post");

            }
        }
    }

}

?>
